package solution;

import java.util.*;

/**
 * @author:whh
 * @date: 2024-02-28 19:37
 * <p></p>
 */
public class Solution {


    /**
     * 求两个字符串中相同字符的个数
     * eg:  输入： aaabbcc  axc
     *      输出： 2
     * @param str1
     * @param str2
     * @return
     */
    public static int fn1(String str1,String str2){

        char[] ch1 = str1.toCharArray();
        char[] ch2 = str2.toCharArray();
        HashSet<Character> set = new HashSet<>();

        for (int i = 0; i < ch1.length; i++) {
            set.add(ch1[i]);
        }


        int same = 0;
        for (int i = 0; i < ch2.length; i++) {

            if(set.contains(ch2[i])){
                same++;
                set.remove(ch2[i]);
            }
        }
        return same;
    }


    /**
     * 求两个字符串的和
     * eg: 输入两个字符串 678   543 从左到右分别对应个、十、百位 计算他们的和
     * 876+45
     * 输出：1221
     * @param str1
     * @param str2
     * @return
     */
    public static int fn2(String str1,String str2){

        char[] ch1 = str1.toCharArray();
        char[] ch2 = str2.toCharArray();
        int len = Math.max(str1.length(),str2.length());
        char[] s1 = new char[len];
        char[]s2=new char[len];
        for (int i = 0; i < len; i++) {
            if(i<ch1.length){
                s1[i]=ch1[i];
            }else {
                s1[i]='0';
            }
            if(i<ch2.length){
                s2[i]=ch2[i];
            }else {
                s2[i]='0';
            }
        }
        //进位标志
        int f = 0;
        //结果
        int result = 0;
        int j =0;
        int k=0;
        for (int i = 0; i < s1.length; i++) {
            int t= Character.getNumericValue(s1[i])+Character.getNumericValue(s2[i]);
            k = t;
            k=k%10+f;
            f = t/10;
            result+=k*Math.pow(10,j);
            j++;
        }
        if(f>0){
            result+= f*Math.pow(10,j);
        }


        System.out.println(result);

        return result;
    }


    /**
     *  给定两个字符串  123 4565  从左到右以此为从高位到低位的数字  计算两个字符串相加的值
     *  例如：
     *  123 + 45 = 148
     * @param str1
     * @param str2
     * @return
     */
    public static int fn4(String str1,String str2){

        int len = Math.max(str1.length(),str2.length());
        char[] s1 = solution(str1, '0', len);
        char[] s2 = solution(str2, '0', len);

        int result=0;
        int f = 0;
        int k = 0;
        int j = 0;
        for (int i = len-1; i >= 0; i--) {

            int t = Character.getNumericValue(s1[i])+Character.getNumericValue(s2[i]);
            k = t;
            k=k%10+f;
            f = t/10;
            result+=k*Math.pow(10,j);
            j++;

        }
        if(f>0){
            result+= f*Math.pow(10,j);
        }
        System.out.println(result);
        return result;


    }


    public static char[] solution(String str,char c,int len){

        char[] ch = new char[len];
        int l = str.length() -1;
        int a = len -1;
        for (int i = a; i >= 0; i--) {
            if(l>=0){
                ch[i]=str.charAt(l);
                l--;
            }else {
                ch[i]=c;
            }
        }

        return ch;

    }




    public static void main(String[] args) {
        fn4("1991", "99");
    }
}
