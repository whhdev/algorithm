package solution;

import java.util.*;

/**
 * @author:whh
 * @date: 2024-02-28 22:27
 * <p></p>
 */
public class LeetCode {

    /**
     * 给定一个整数数组 nums 和一个整数目标值 target，
     * 请你在该数组中找出 和为目标值 target  的那 两个 整数，并返回它们的数组下标。
     *
     * nums = [2,7,11,15], target = 9
     * @param nums
     * @param target
     * @return
     */
    public static int[] fn1(int[]nums,int target){

        Map<Integer,Integer> map = new HashMap<>();

        for (int i = 0; i < nums.length; i++) {

            boolean b = map.containsKey(target - nums[i]);
            if(b){
                return new int[]{i,map.get(target-nums[i])};
            }else {

                map.put(nums[i], i);
            }
        }



        return new int[]{};
    }


    /**
     * 你一个字符串数组，请你将 字母异位词 组合在一起。可以按任意顺序返回结果列表。
     *
     * 字母异位词 是由重新排列源单词的所有字母得到的一个新单词。
     * 你一个字符串数组，请你将 字母异位词 组合在一起。可以按任意顺序返回结果列表。
     *
     * 字母异位词 是由重新排列源单词的所有字母得到的一个新单词。
     *
     * 输入: strs = ["eat", "tea", "tan", "ate", "nat", "bat"]
     * 输出: [["bat"],["nat","tan"],["ate","eat","tea"]]
     *
     *
     * 关键的：：：字母移位此中包含的字符都是相同的
     * @return
     */
    public static String[] fn2(String[] arr){

        Map<String, List<String>> map = new HashMap<>();

        for (String s : arr) {

            char[] array = s.toCharArray();
            //移位字符串排序后都一样
            Arrays.sort(array);
            //排序后的字符串作为key
            String key = new String(array);

            List<String> value = map.getOrDefault(key, new ArrayList<>());

            value.add(s);

            map.put(key,value);


        }

        return new String[]{};
    }


    /**
     *
     * 给定一个未排序的整数数组 nums ，找出数字连续的最长序列（不要求序列元素在原数组中连续）的长度。
     *
     * 输入：nums = [100,4,200,1,3,2]
     * 输出：4
     * 解释：最长数字连续序列是 [1, 2, 3, 4]。它的长度为 4。
     *
     *
     * 使用set()构建 hash table，并非总是依赖 dict，应该根据实际选取；
     * set 天然的适合去重，这道题认识到去重是很重要的；
     * 如何判断一个子序列首部？num-1 不在 set 里！
     * 如何判断一个子序列尾部？num+1 不在 set 里！
     * @param nums
     * @return
     */
    public static int fn3(int[]nums){

        HashSet<Integer> set = new HashSet<>();

        for (int num : nums) {
            set.add(num);
        }


        int max = 0;
        for (int num : nums) {
            int curLen = 0;
            //确定子序列的首部
            if(!set.contains(num-1)){
                int cur = num;
                while (set.contains(cur)){
                    cur+=1;
                    curLen++;
                }

            }

            max = Math.max(curLen,max);


        }


        System.out.println(max);
        return max;
    }


    public static void main(String[] args) {
        int[] num2 = {200,1,200,3,4,5,900,6,8};
        fn3(num2);
    }
}
